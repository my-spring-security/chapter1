package com.lh0811.security.chapter1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class HelloController {


    @GetMapping("/hello")
    private String hello() {
        return "Hello SpringSecurity！！!";
    }

}
